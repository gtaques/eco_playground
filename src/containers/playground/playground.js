import React, {Component} from 'react';
import TasksUser from '../../components/tasks/tasks_user/tasks_user';
import './playground.css';

class Playground extends Component {
    render() {
        return (
            <div>
                <TasksUser />
            </div>
        )
    }
}

export default Playground;