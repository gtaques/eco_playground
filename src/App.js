import React, { Component } from 'react';
import logo from './logo.svg';
import Playground from './containers/playground/playground';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div>
          <Playground />
        </div>
      </div>
    );
  }
}

export default App;
