import React from 'react';
import './taskcard.css';

const TaskCard = (props) => (
    <div className="task-card">
        <div className="logo-card">
            <img src=""/>
        </div>
        <div className="content-card">
            <h4>Title</h4>
            <h5>Sub-title Sub-title Sub-title</h5>
            <button>Details</button>
            <h5>Status</h5>
            <h6>Due Date</h6>
        </div>
    </div>
)

export default TaskCard;