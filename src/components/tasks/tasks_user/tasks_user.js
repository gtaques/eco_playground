import React, {Component} from 'react';
import TaskCard from '../task_card/taskcard';
import './tasks_user.css';

class TasksUser extends Component {
    state = {

    }
    render() {
        return(
            <div className="tasks-user">
                <div className="tasks-user-header">
                    <h3>My Tasks</h3> 
                </div>
                <div className="task-cards">
                    <TaskCard />
                    <TaskCard />
                    <TaskCard />
                    <TaskCard />
                </div>
            </div>
        )
    }
}

export default TasksUser;